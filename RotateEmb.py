from transformers import GPTNeoXConfig, GPTNeoXModel

# Initializing a GPTNeoX gpt-neox-20b style configuration
configuration = GPTNeoXConfig()

configuration.num_hidden_layers = 12
# Initializing a model (with random weights) from the gpt-neox-20b style configuration
model = GPTNeoXModel(configuration)
# Accessing the model configuration
configuration = model.config

self.model = GPTNeoXForCausalLM.from_pretrained("EleutherAI/gpt-neox-20b", config=self.config)
self.tokenizer = GPTNeoXTokenizerFast.from_pretrained("openai-community/gpt2")


